﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone5Minesweeper
{
    public partial class GameField : Form
    {
        //Mason Blaut - This is my own work

        private int seconds;
        public GameField()
        {

            //InitializeComponent();
            //populateGrid();
            checkGrid();
        }

        public GameField(int start)
        {
            InitializeComponent();
            timer1.Start();
            populateGrid(start);
            checkGrid();
            
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            seconds++;
            labelTime.Text = seconds.ToString();

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void Label5_Click(object sender, EventArgs e)
        {

        }
    }
}
