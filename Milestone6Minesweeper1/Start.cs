﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone5Minesweeper
{
    public partial class Start : Form
    {
        public Start()
        {
            InitializeComponent();
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            GameField form1 = new GameField(8);
            form1.Show();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            GameField form1 = new GameField(12);
            form1.Show();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            GameField form1 = new GameField(16);
            form1.Show();
        }
    }
}
